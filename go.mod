module gitlab.com/vikanezrimaya/feedeater

go 1.13

require (
	github.com/andyleap/microformats v0.0.0-20150523144534-25ae286f528b
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.15
	github.com/labstack/gommon v0.3.0
	willnorris.com/go/microformats v1.0.0
)
